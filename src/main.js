import Vue from 'vue'
import App from './App'
import router from './router'

import * as Sentry from '@sentry/browser';
import { Vue as VueIntegration } from '@sentry/integrations';

Sentry.init({
  dsn: 'http://ffc10583f3d443159bfb54819f6a4e29@localhost:9000/13',
  integrations: [new VueIntegration({Vue, attachProps: true})],
});

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
